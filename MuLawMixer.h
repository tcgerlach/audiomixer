#include <cstdio>
#include <string>
#include <fstream>

#ifdef __WINDOWS__
	#include "stdafx.h"
#endif

using namespace std;

#ifndef __MULAWMIXER_H__
#define __MULAWMIXER_H__

/*
 * Simple class for mixing audio files. 
 * By Thomas C. Gerlach
 */
class CMuLawMixer
{	
	public:
		// Input: 
		//		string backgroundFile - audio for background
		//		string foregroundFiles[] - array of forground audio
		//		int numFiles - number of files in foreground array
		//		string backgroundFile - output file
		//		int endTime - how long to continue playing background audio after foreground is finished
		//		int beginTime - how long to pause before inserting foreground audio
		// Purpose:
		//		Concatenate all forgroundFiles into a contiguous audio file and mix with backgroundFile.
		//		Output the resulting audio to outputFile
		// Preconditions:
		//		All audio files are 8K mu-law
		//		It is assumed that the volume on all input files are at the desired amp.  No leveling will occur.
		// Returns:
		//		0 on success
		//		1 if unable to open output file
		// Output:
		//		Warnings and errors sent to stderr
		static int Mix(string backgroundFile, string foregroundFiles[], int numFiles, string outputFile, int beginTime = 0, int endTime = 0);

	private:
		static short decode(unsigned char mulaw);
		static char  encode(int pcm);
};
#endif /* __MULAWMIXER_H__ */

