This is a simple class to mix multiple files into a single file
---

**Input: **  
	string backgroundFile - audio for background  
	string foregroundFiles[] - array of forground audio  
	int numFiles - number of files in foreground array  
	string backgroundFile - output file  
	int endTime - how long to continue playing background audio after foreground is finished  
	int beginTime - how long to pause before inserting foreground audio  

**Purpose:**  
	Concatenate all forgroundFiles into a contiguous audio file and mix with backgroundFile.  
	Output the resulting audio to outputFile  

**Preconditions:**  
	All audio files are 8K mu-law  
	It is assumed that the volume on all input files are at the desired amp.  No leveling will occur.  

**Returns:**  
	0 on success  
	1 if unable to open output file  

**Output:**  
	Warnings and errors sent to stderr  
