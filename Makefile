CPP=g++
FLAGS=-O3 -Wall -c
OUT=mixer.o

all: MuLawMixer.cpp
	${CPP} ${FLAGS} MuLawMixer.cpp -o ${OUT}

clean:
	@rm ${OUT}
