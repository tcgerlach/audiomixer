#ifdef __WINDOWS__
	#include "stdafx.h"
#endif

#include "MuLawMixer.h"

int CMuLawMixer::Mix(string backgroundFile, string foregroundFiles[], int numFiles, string outputFile, int beginTime, int endTime) 
{		
	//***********************************************************
	// read 1 byte from background
	// if background exhausted use 0
	// read 1 byte from forground
	// if forground exhausted, goto next file
	// if no more foreground files use 0 for byte
	// convert samples to PCM
	// output byte = (sampleA + sampleB) / 2
	// write output byte to file
	// while forground or background has more data goto next byte
	//***********************************************************

	bool done = false;		// true when all streams exhausted
	bool bggood = true;		// false when background stream exhausted
	bool fggood = true;		// false when foreground stream exhausted

	int maxBytesPastForeground = endTime * 8000;	// how many bytes to go past the end of the foreground
	int curBytesPastForeground = 0;

	int maxBytesPastBegining = beginTime * 8000;	// how many bytes to go past the beginning of the foreground
	int curBytesPastBegining = 0;
	bool useBackgroundOnly = false;		// true at the beginning when there is no foreground

	// create output stream - if this fails, return 1
	ofstream *outstream = new ofstream();
	outstream->open(outputFile.c_str(), std::ios_base::binary);
	if (!outstream->is_open()) {
		if (outstream != NULL) {
			delete outstream;
		}
		fprintf(stderr,"%s%s\n", "ERROR: Unable to create output file - ", outputFile.c_str());
		return 1;	// can't continue if outputfile bad
	}

	// create input streams - if these fail, output a warning
	ifstream *bgstream = new ifstream();
	bgstream->open(backgroundFile.c_str(), std::ios_base::binary);	
	if (!bgstream->is_open()) {		
		fprintf(stderr,"%s%s\n", "WARNING: Unable to find background file - ", backgroundFile.c_str());
	}
	
	ifstream *fgstream = new ifstream();
	int fgcounter = 0;
	fgstream->open(foregroundFiles[fgcounter].c_str(), std::ios_base::binary);
	if (!fgstream->is_open()) {
		fprintf(stderr,"%s%s\n", "WARNING: Unable to find foreground file - ", foregroundFiles[fgcounter].c_str());
	}
	
	while (!done) {
		// get sample from background
		int bgsample = 0;
		
		if (fggood == false) {
			++curBytesPastForeground;
		}

		if (curBytesPastForeground <= maxBytesPastForeground && bgstream->good()) {
			bgsample = (short)bgstream->get();
		} else {
			bggood = false;			
		}

		// get sample from foreground
		int fgsample = 0;
		if (++curBytesPastBegining <= maxBytesPastBegining) {
			useBackgroundOnly = true;
			// wait till we pass the desired beginning background before mixing
		} else {
			useBackgroundOnly = false;
			if (fgstream->good()) {
				fgsample = (short)fgstream->get();
			} else {
				// current file empty, go to next file
				if (fggood && ++fgcounter < numFiles) {
					fgstream->close();	
					delete fgstream;
					fgstream = new ifstream();
					fgstream->open(foregroundFiles[fgcounter].c_str(), std::ios_base::binary);				
					if (!fgstream->is_open()) {
						fprintf(stderr,"%s%s\n", "WARNING: Unable to find foreground file - ", foregroundFiles[fgcounter].c_str());
					}

					if (fgstream->good()) {
						fgsample = (short)fgstream->get();
					} else {					
						fggood = false;			
					}
				} else {				
					fggood = false;
				}
			}
		}	

		// mix foreground and background
		int result;
		if (fggood && bggood && !useBackgroundOnly) {
			// must convert mulaw to pcm, add samples, divide by 2, then convert back to mulaw
			result = encode((decode(bgsample) + decode(fgsample)) / 2);
		} else if (bggood) {
			result = bgsample;
		} else if (fggood) {
			result = fgsample;		
		} else if (!bggood && !fggood){
			done = true;
		}

		// output result
		if (!done) {
			outstream->put((char)result);
		}
	}

	// close streams
	bgstream->close();
	fgstream->close();
	outstream->close();

	// cleanup memory
	delete bgstream;
	delete fgstream;
	delete outstream;

	return 0;
}

// Convert mulaw byte to PCM
short CMuLawMixer::decode(unsigned char mulaw)
{
    //Flip all the bits
    mulaw = (unsigned char)~mulaw;

    //Pull out the value of the sign bit
    int sign = mulaw & 0x80;

    //Pull out and shift over the value of the exponent
    int exponent = (mulaw & 0x70) >> 4;

    //Pull out the four bits of data
    int data = mulaw & 0x0f;

    //Add on the implicit fifth bit (we know 
    //the four data bits followed a one bit)
    data |= 0x10;

    /* Add a 1 to the end of the data by shifting over and adding one. Why?
     * Mu-law is not a one-to-one function. There is a range of values that all
     * map to the same mu-law byte. Adding a one to the end essentially adds a
     * "half byte", which means that the decoding will return the value in the
     * middle of that range. Otherwise, the mu-law decoding would always be
     * less than the original data. 
	 */
    data <<= 1;
    data += 1;

    /* Shift the five bits to where they need
     * to be: left (exponent + 2) places
     * Why (exponent + 2) ?
     * 1 2 3 4 5 6 7 8 9 A B C D E F G
     * . 7 6 5 4 3 2 1 0 . . . . . . . <-- starting bit (based on exponent)
     * . . . . . . . . . . 1 x x x x 1 <-- our data
     * We need to move the one under the value of the exponent,
     * which means it must move (exponent + 2) times
     */
    data <<= exponent + 2;

    //Remember, we added to the original,
    //so we need to subtract from the final
    data -= 0x84;

    //If the sign bit is 0, the number 
    //is positive. Otherwise, negative.
    return (short)(sign == 0 ? data : -data);
}

// Convert from PCM to mulaw
char CMuLawMixer::encode(int pcm) //16-bit
{
    //Get the sign bit. Shift it for later 
    //use without further modification
    int sign = (pcm & 0x8000) >> 8;

    //If the number is negative, make it 
    //positive (now it's a magnitude)
    if (sign != 0)
        pcm = -pcm;

    //The magnitude must be less than 32635 to avoid overflow
    if (pcm > 32635) pcm = 32635;

    //Add 132 to guarantee a 1 in 
    //the eight bits after the sign bit
    pcm += 0x84;

    /* Finding the "exponent"
    * Bits:
    * 1 2 3 4 5 6 7 8 9 A B C D E F G
    * S 7 6 5 4 3 2 1 0 . . . . . . .
    * We want to find where the first 1 after the sign bit is.
    * We take the corresponding value from
    * the second row as the exponent value.
    * (i.e. if first 1 at position 7 -> exponent = 2) */
    int exponent = 7;

    //Move to the right and decrement exponent until we hit the 1
    for (int expMask = 0x4000; (pcm & expMask) == 0; 
         exponent--, expMask >>= 1) { }

    /* The last part - the "mantissa"
    * We need to take the four bits after the 1 we just found.
    * To get it, we shift 0x0f :
    * 1 2 3 4 5 6 7 8 9 A B C D E F G
    * S 0 0 0 0 0 1 . . . . . . . . . (meaning exponent is 2)
    * . . . . . . . . . . . . 1 1 1 1
    * We shift it 5 times for an exponent of two, meaning
    * we will shift our four bits (exponent + 3) bits.
    * For convenience, we will actually just shift
    * the number, then and with 0x0f. */
    int mantissa = (pcm >> (exponent + 3)) & 0x0f;

    //The mu-law byte bit arrangement 
    //is SEEEMMMM (Sign, Exponent, and Mantissa.)
    unsigned char mulaw = (unsigned char)(sign | exponent << 4 | mantissa);

    //Last is to flip the bits
    return (unsigned char)~mulaw;
}
